!!****if* source/physics/sourceTerms/KromeChemistry/pchem_mapNetworkToSpecies
!!
!!  NAME
!!
!!   pchem_mapNetworkToSpecies
!!
!!  SYNOPSIS
!!
!!   call pchem_mapNetworkToSpecies(character krome_species_name,
!!                                  integer, intent(OUT) :: specieOut)
!!
!!  DESCRIPTION
!!
!!   Maps network indices required by chemistry to species generated in Flash.h
!!
!!  ARGUMENTS
!!
!!   krome_species_name --> name of species define in krome_user_commons.f90
!!   specieOut --> integer corresponding to the same species in Flash.h
!!
!!  Daniel Seifried and KROME Team 2013
!!  mofified by Christoph Federrath, 2020
!!
!!****

subroutine pchem_mapNetworkToSpecies(krome_species_name, specieOut)

  use Driver_interface, ONLY: Driver_abortFlash
  use Logfile_interface, ONLY: Logfile_stampMessage

  implicit none

#include "Flash.h"

  character(len=16) :: krome_species_name
  integer, intent(OUT) :: specieOut

  !! Local variable for error diagnostics
  character(len=256) :: internalFile

  ! Add species names according to the name convention given in krome:get_names and Flash.h
  select case(krome_species_name)
#KROME_cases
  case default
  !!Abort
  call Logfile_stampMessage('ERROR in pchem_mapNetworkToSpecies')
  write(internalFile, *) '[pchem_mapNetworkToSpecies] Name ', krome_species_name,' cannot be mapped to a Flash.h species'
  call Logfile_stampMessage(internalFile)
  call Driver_abortFlash(internalFile)
  end select

  return

end subroutine pchem_mapNetworkToSpecies
