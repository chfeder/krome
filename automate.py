#!/usr/bin/env python
# -*- coding: utf-8 -*-
# written by Christoph Federrath, 2020
import argparse
from tempfile import mkstemp
from shutil import move, copyfile
import numpy as np
import os


# === function to search for a string pattern at the start of a line, and to replace that line ===
def replace_line_in_file(filename, search_str, new_line):
    debug = True
    fh, tempfile = mkstemp()
    with open(tempfile, 'w') as ftemp:
        with open(filename, 'r') as f:
            found_line = False
            for line in f:
                # replace
                if line.find(search_str)==0:
                    found_line = True
                    if debug==True: print(filename+": found line   : "+line.rstrip())
                    line = new_line+"\n"
                    if debug==True: print(filename+": replaced with: "+line.rstrip())
                # add lines to temporary output file
                ftemp.write(line)
    os.remove(filename)
    move(tempfile, filename)
    os.chmod(filename, 0o644)


# ===== MAIN Start =====
# ===== the following applies in case we are running this in script mode =====
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Automate KROME.')
    parser.add_argument("-options_file", "--options_file", type=str, help="Name of Krome options file.", default="tests/collapseCO/options.opt")
    parser.add_argument("-flash", "--flash", action='store_true', help="Create patch files for FLASH.", default=False)
    args = parser.parse_args()

    # prepare
    flash_opts = ""
    if args.flash: flash_opts = " -flash -compact"
    cmd = "python2 krome -clean -unsafe -options "+args.options_file+flash_opts
    print(cmd)
    os.system(cmd)

    # compile if this is a test
    if not args.flash:
        cmd = "cd build; make gfortran"
        print(cmd)
        os.system(cmd)

    # fix default Makefile for gfortran (only has to be done once, so we exit before the following)
    exit()
    filename = 'tests/Makefile'
    # make a backup copy
    print("Copying "+filename+" to "+filename+"_backup as backup.")
    copyfile(filename, filename+"_backup")
    # overwrite lines
    replace_line_in_file(filename, "gfortran: fc = gfortran", "gfortran: fc = gfortran-mp-10")
    replace_line_in_file(filename, "gfortran: switch = -ffree-line-length-none", "gfortran: switch = -ffree-line-length-none -fallow-argument-mismatch")

# ===== MAIN End =====
